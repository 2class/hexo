---
title: 免费临时服务器分享
date: 2022-08-30 13:49:19
tags:
    - vps
    - 服务器
categories: 免费资源
---
免费临时服务器是一种可以免费使用一段时间的云服务器，适合临时使用或测试。目前市场上有很多云计算平台提供免费临时服务器，如Google Cloud、AWS、Azure等。用户只需注册账号、选择机型和配置，即可在短时间内获取一个可用的云服务器。虽然免费临时服务器的配置和使用时间有限，但对于一些短期的项目或测试来说，仍然是一个非常实用的选择。
<!--more-->
1.http://netbooks.networkmedicine.org/
免费的在线Jupyter Notebook，无需注册即可使用，无ROOT权限
8核心32G运行内存
闲置超过20分钟或运行大于1小时自动关闭

2.https://courses.nvidia.com/courses/course-v1:DLI+S-ES-01+V1/
免费的在线Jupyter Notebook，注册NVIDIA账户即可使用，4小时后自动关闭，有ROOT
GPU:Tesla T4

3.https://gitpod.io/
使用Github或Google账号直接登录即可使用，一次可以运行3小时，有ROOT

4.https://mybinder.org/v2/git/https%3A%2F%2Fgithub.com%2Faanksatriani%2Fbinder72cpu.git/main
点击链接即可使用，无ROOT

5.https://app.community.saturnenterprise.io/
免费的在线Jupyter Notebook，注册账户即可使用，有月时间限制


