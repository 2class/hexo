---
title: Cloudflare测速SpeedTest自选IP
date: 2022-08-25 15:50:03
tags:
	- Cloudflare
	- 自选IP
	- Speedtest
        - 测速
categories: 软件资源
---
Cloudflare是一家提供CDN和网络安全服务的公司，其CDN服务可以加速网站的访问速度。测试Cloudflare CDN延迟和速度，获取最快的IPv4 / IPv6地址可以帮助网站管理员优化网站的性能，提高用户体验。通过测试，可以找到最快的节点，从而更新DNS记录，实现更快的访问速度。
![](https://img.baxx.eu.org/202301272256903.png)
<!--more-->
# 下载运行
1. 下载编译好的可执行文件 [蓝奏云](https://pan.lanzouv.com/b0742hkxe) / [Github](https://github.com/XIU2/CloudflareSpeedTest/releases) 并解压。  
2. 双击运行 `CloudflareST.exe` 文件（Windows 系统），等待测速完成...  

Github仓库:https://github.com/XIU2/CloudflareSpeedTest
