---
title: 免费获取Bilibili的视频链接域名
date: 2022-11-22 12:06:59
tags:
	- Bilibili
	- Domain
	- 域名
	- 免费域名
categories: 免费资源
---
BiliBili是一家中国知名的在线视频平台，用户可以在该平台上观看和分享各种类型的视频内容，包括动画、游戏、影视、音乐等。哔哩哔哩还提供了带有ICP备案的免费的子域名服务，用户可以通过该服务将自己的网站或服务器IP地址与哔哩哔哩的子域名进行关联，从而实现通过哔哩哔哩子域名访问自己的网站或服务器的功能。这对于个人博客、小型网站等来说是非常实用的。
<!--more-->
把下面链接数字替换为你的ip即可
可以理解为A解析
http://xy38x60x36x131xy.mcdn.bilivideo.cn/
例如：你的ip是192.168.1.1
就把上面的链接改为：
http://xy192x168x1x1xy.mcdn.bilivideo.cn/

ip134744072.mobgslb.tbcache.com -> 8.8.8.8
ip1.mobgslb.tbcache.com-> 1.0.0.0

转自：Testflight 频道
🤖 投稿：@ZaiHuabot
📣 频道：@TestFlightCN
