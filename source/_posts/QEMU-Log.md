---
title: QEMU新建空白镜像与启动的命令
date: 2022-10-15 12:06:59
tags:
	- QEMU
	- QEMU新建空白镜像
	- QEMU启动
	- QEMU命令
categories: 技术教程
---
QEMU是一个开源的通用机器和用户空间仿真器和虚拟机，它可以在多种平台上运行，并支持多种处理器架构。QEMU主要用于虚拟化和仿真，它可以模拟不同的硬件平台和操作系统，并提供了丰富的虚拟化功能，包括虚拟化网络、磁盘、USB、串口等。QEMU还支持多种虚拟机格式，包括QEMU自己的格式、VMware格式、VirtualBox格式等，能够很好地满足不同用户的需求。
<!--more-->
# 新建空白镜像
```
qemu-img create -f qcow2 /root/w.qcow2 5G
```
# 启动
```
qemu-system-x86_64 -m 512 -boot c -hda /root/windowsxp.qcow2 -net nic,model=rtl8139 -net user -soundhw ac97 -vga cirrus -vnc :1 -net user,hostfwd=tcp::3389-:3389 -localtime
```
