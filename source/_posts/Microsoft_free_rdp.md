---
title: Microsoft免费临时远程桌面
date: 2022-08-22 14:25:12
tags:
	- Microsoft
	- 微软
	- RDP
	- VPS
	- 免费VPS
categories: 免费资源
---
Microsoft Learn是一项非常好的免费学习资源平台，提供了各种主题的课程和实验室环境。其中，沙盒就是一种非常方便的工具，可以让学习者在虚拟机中进行实验和练习。每个沙盒可以免费使用2小时，使用后数据会自动清除，而且每天还可以领取10个沙盒，非常实用。通过使用沙盒，学习者可以在安全的环境中进行实践，提高技能和经验，而且不会带来实际的风险。总
<!--more-->
# 教程
1.打开 https://docs.microsoft.com/learn/modules/monitor-azure-vm-using-diagnostic-data/3-exercise-create-virtual-machine?activate-azure-sandbox=true 登录你的微软账户
2.点击激活沙盒,验证手机号
3.复制命令到Azure Cloud Shell终端
```
curl -skLO is.gd/azurewinvmplus ; chmod +x azurewinvmplus ; ./azurewinvmplus
```
Win11Only
```
 curl -skLO is.gd/azurewin11vm ; chmod +x azurewin11vm ; ./azurewin11vm    
```
4.输入数字选择区域,操作系统以及配置
5.等待1-5分钟
6.RDP信息会打印在终端上

默认用户名:```azureuser```
默认密码:```WindowsPassword@001```

![](https://files.baxx.eu.org/img/202208221431788.png)
![](https://files.baxx.eu.org/img/202208221437732.png)

Github项目:https://github.com/kmille36/Windows-11-VPS

其它沙盒地址
```
1H: https://docs.microsoft.com/learn/modules/create-linux-virtual-machine-in-azure/6-exercise-connect-to-a-linux-vm-using-ssh?activate-azure-sandbox=true

1H: https://docs.microsoft.com/learn/modules/build-a-web-app-with-mean-on-a-linux-vm/3-create-a-vm?activate-azure-sandbox=true
```
在线RDP
```
2H：https://docs.microsoft.com/en-us/learn/modules/implement-common-integration-features-finance-ops/10-exercise-1
```
清理磁盘空间命令
```
taskkill /f /im sqlservr.exe
taskkill /f /im Batch.exe
taskkill /f /im w3wp.exe
cd C:\
rmdir /s /q AOSService
rmdir /s /q DumpPath
rmdir /s /q DynamicsDiagnostics
rmdir /s /q DynamicsSDK
rmdir /s /q DynamicsTools
rmdir /s /q EmptyDataset
rmdir /s /q FinancialReporting
rmdir /s /q Labs
rmdir /s /q PerfSDK
rmdir /s /q RetailSDK
rmdir /s /q RetailSelfService
rmdir /s /q RetailServer
rmdir /s /q RetailStorefront
```
