---
title: Microsoft365 E3/E5/A1订阅 开通Loop的方法
date: 2023-03-22 12:06:59
tags:
	- microsoft
	- e3
	- loop
	- ai
  - e5
categories: 技术教程
---
Loop 是一款 Microsoft 推出的视频会议工具，可与 Microsoft Teams 集成。使用 Loop，您可以轻松地创建和共享会议链接，无需安装任何应用程序或浏览器插件。
<!--more-->
![](https://img.baxx.eu.org/202303232044113.png)
# 前置条件
必须拥有 Microsoft 365 E3/E5/A1 订阅。
必须有管理员权限。
必须配置 Exchange 邮箱。
# 步骤
https://dev.loop.microsoft.com/
### 1.创建一个安全组，并添加成员。已有安全组的可以忽略。
### 2. 创建一个策略，包含上一步创建的安全组。

官方教程:

创建安全组:
https://learn.microsoft.com/en-us/microsoft-365/admin/email/create-edit-or-delete-a-security-group?view=o365-worldwide

创建策略:
https://learn.microsoft.com/en-us/sharepoint/manage-loop-components

视频教程
https://www.youtube.com/watch?v=MX_rFr-OVXM

https://hostloc.com/forum.php?mod=viewthread&tid=1151043
