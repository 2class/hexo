---
title: CFPCloudflarePartner面板Cloudflare-CNAME-Setup安装
date: 2022-08-22 12:06:59
tags:
	- cfp
	- cloudflarepartner
	- Cloudflare-CNAME-Setup
	- Cloudflare
categories: 软件资源
---
Cloudflare Hosting Partner是一项合作计划，为用户提供可视化面板，免费使用CNAME接入。该计划提供工具和资源，为合作伙伴管理和扩展客户的Web应用程序提供便利。用户可以轻松访问高性能、高可用性和安全的网络服务，同时享受更好的用户体验。
<!--more-->
# 演示
![](https://files.baxx.eu.org/img/202208221215853.png)
# 下载
原项目地址(作者已删除)：
https://github.com/ZE3kr/Cloudflare-CNAME-Setup
Onedrive 下载:
https://onedrive.baxx.eu.org/api/raw/?path=/Cloudflare-CNAME-Setup.zip

# 安装

编辑config.php
```
<?php

define('HOST_KEY', '');
define('HOST_MAIL', '');
// $page_title = ""; // Optional. Should not use HTML special character.
// $tlo_path = ''; // Optional. The installation path for this panel, ending with '/'. Required for HTTP/2 Push.
// $is_debug = true; // Enable debug mode

```
